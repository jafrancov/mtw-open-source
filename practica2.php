<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Registro de teléfonos</title>
</head>
<body>
    <?php
    $registrado = False;
    if ($_POST) {
        $numeroTelefono = $_POST['numero_telefono'];
        $regexTelefono = '/^\d{10}$/';
        if (!preg_match($regexTelefono, $numeroTelefono)) {
            ?>
            <ul>
                <span style="color: indianred"><li>Número <strong><?php echo $numeroTelefono ?></strong> inválido</li></span>
            </ul>
            <?php
        } else {
            $numerosRegistrados = file_put_contents('numeros_registrados.txt', $numeroTelefono . PHP_EOL, FILE_APPEND);
            $registrado = True;
            ?>
            <ul>
                <span style="color: forestgreen"><li>Número registrado</li></span>
            </ul>
            <?php
        }
    }
    ?>
    <form method="post" action="">
        <p>
            <label for="id_numero_telefono">Número de teléfono</label>
            <input type="text" maxlength="10" name="numero_telefono" id="id_numero_telefono" placeholder="1234567890">
        </p>
        <p>
            <input type="submit" value="Registrar">
        </p>
    </form>
    <?php
    if($registrado){
        ?>
        <p>Números registrados: </p>
        <p><?php
            $numeros_registrados = file_get_contents('numeros_registrados.txt');
            echo str_replace("\n","<br>", $numeros_registrados);
            ?></p>
        <?php
    }
    ?>
</body>
</html>