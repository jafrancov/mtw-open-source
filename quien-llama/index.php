<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Consulta quién te llama</title>

    <!-- Bootstrap core CSS -->
    <link href="https://getbootstrap.com/docs/4.2/dist/css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">
</head>
<body class="text-center">
<div class="form-consulta" action="#">
    <p style="color: #563d7d;"><i class="fas fa-blender-phone fa-5x"></i></p>
    <h1 class="h3 mb-3 font-weight-normal">¿Quién llama?</h1>
    <label for="id_telefono" class="sr-only">Número telefónico</label>
    <input type="text" id="id_telefono" maxlength="10" class="form-control" placeholder="Teléfono: 4621234567" required
           autofocus>
    <div class="checkbox mb-3">
    </div>
    <button class="btn btn-lg btn-primary" id="id-consultar-telefono">&nbsp;&nbsp;&nbsp;Consultar&nbsp;&nbsp;&nbsp;</button>
    <p class="mt-5 mb-3 text-muted">&copy; 2019</p>
</div>


<div class="modal fade" id="consultaModal" tabindex="-1" role="dialog" aria-labelledby="consultaModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="consultaModalLabel">Encontraste un nuevo spam</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>¿Lo registramos?</p>
                <form id="id-registrar-telefono">
                    <div class="form-group">
                        <label for="nuevo-telefono" class="col-form-label">Teléfono:</label>
                        <input type="text" class="form-control" required readonly name="nuevo-telefono" id="id-nuevo-telefono" />
                    </div>
                    <div class="form-group">
                        <label for="quien-llama" class="col-form-label">¿Quién llama?</label>
                        <input type="text" class="form-control" required name="quien-llama" id="id-quien-llama" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btn_registrar" class="btn btn-primary">Registrar</button>
            </div>
        </div>
    </div>
</div>

<script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
<script src="https://getbootstrap.com/docs/4.0/dist/js/bootstrap.min.js" crossorigin="anonymous"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
    $('#id-consultar-telefono').click(function () {
        var telefono = $("#id_telefono");
        $.getJSON("consulta.php?telefono=" + telefono.val(), function (results) {
            if (results.error) {
                $('#consultaModal').modal('hide');
                swal("¡ERROR!", results.mensaje, "error");
            } else {
                if (results.encontrado) {
                    swal("Reportado como '" + results.quien_llama + "'", "Encontramos resgistrado el número " + telefono.val() + " en nuestra base de datos.");
                } else {
                    var consulta = $('#consultaModal');
                    consulta.modal('show');
                    consulta.find('.modal-body input#id-nuevo-telefono').val(telefono.val());
                }
            }
        });
    });

    $('#btn_registrar').click(function () {
        var nuevoTelefono = $("#id-registrar-telefono input#id-nuevo-telefono").val();
        var nuevoQuienLlama = $("#id-registrar-telefono input#id-quien-llama").val();
        $.post("registrar.php", $("#id-registrar-telefono").serialize(), function( results ) {
            $('#consultaModal').modal('hide');
            if(results.error) {
                swal("¡ERROR!", results.mensaje, "error");
            } else if (!results.insertado) {
                swal("¡ERROR!", "El número ya está registrado en nuestra base de datos", "error");
            } else {
                swal("¡Nuevo número reportado!", 'Registraste el número ' + nuevoTelefono + ' como "' + nuevoQuienLlama + '"', "success");
            }
            $("#id_telefono").val('');
            nuevoTelefono.val('');
            nuevoQuienLlama.val('');
        }, "json");
    })
</script>
</body>
</html>
