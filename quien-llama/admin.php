<?php
session_start();
if (!isset($_SESSION["email"])) {
    header('Location: login.php');
    exit;
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Admin de Spam</title>

    <!-- Bootstrap core CSS -->
    <link href="https://getbootstrap.com/docs/4.2/dist/css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">


    <style>
        body {
            background-color: #f5f5f5;
        }

        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }

        .container {
            max-width: 960px;
        }

        .lh-condensed { line-height: 1.25; }
    </style>
    <!-- Custom styles for this template -->
</head>
<body>
<nav class="navbar navbar-expand-sm navbar-light bg-light">
    <a class="navbar-brand" href="#">Quién llama</a>
    <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId"
            aria-controls="collapsibleNavId"
            aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsibleNavId">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <li class="nav-item active">
                <a class="nav-link disabled" href="#">Bienvenido <?php echo $_SESSION["nombre"]; ?></a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item nav-right">
                <a href="./salir.php" class="nav-link">Salir</a>
            </li>
        </ul>
    </div>
</nav>

<div class="container">
    <div class="py-5 text-center">
        <p style="color: #563d7d;"><i class="fas fa-blender-phone fa-5x"></i></p>
        <h2>Administración de números reportados</h2>
    </div>

        <table id="example" class="table table-striped table-bordered" style="width:100%">
            <thead>
            <tr>
                <th>Teléfono</th>
                <th>Quién llama</th>
                <th>Opciones</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
            <tr>
                <th>Teléfono</th>
                <th>Quién llama</th>
                <th>Opciones</th>
            </tr>
            </tfoot>
        </table>

    <div class="modal fade" id="consultaModal" tabindex="-1" role="dialog" aria-labelledby="consultaModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="consultaModalLabel">Editar registro</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>

    <footer class="my-5 pt-5 text-muted text-center text-small">
        <p class="mb-1">&copy; 2017-2018 Company Name</p>
        <ul class="list-inline">
            <li class="list-inline-item"><a href="#">Privacy</a></li>
            <li class="list-inline-item"><a href="#">Terms</a></li>
            <li class="list-inline-item"><a href="#">Support</a></li>
        </ul>
    </footer>
</div>
<script
    src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
<script src="https://getbootstrap.com/docs/4.0/dist/js/bootstrap.min.js" crossorigin="anonymous"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>'
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var datatable = $('#example').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "getData.php",
            "columnDefs": [
                {
                    "render": function ( data, type, row ) {
                        return '' +
                            '<a href="editar.php?telefono=' + data + '" data-toggle="modal" data-target="consultaModal" class="btn btn-primary editar"><i class="fas fa-pencil-alt"></i></a>' +
                            '<a href="eliminar.php?telefono=' + data + '" class="btn btn-danger eliminar"><i class="fas fa-trash-alt"></i></a>';
                    },
                    "targets": 2
                },
            ]
        });

        datatable.on('draw.dt', function () {
            $('.eliminar').click(function(event) {
                event.preventDefault();
                $.getJSON($(this).attr("href"), function (results) {
                    if (results.eliminado) {
                        swal("Se eliminó el teléfono " + results.telefono, "", "success")
                            .then(() => {
                                datatable.ajax.reload();
                            });
                    } else {
                        swal("El teléfono no existe", "", "error");
                    }
                });
            });

            $('.editar').click(function(event) {
                var consulta = $('#consultaModal');
                consulta.modal('show');
                consulta.find('.modal-body').load($(this).attr('href'));
            });

        });
    } );




</script>
</html>
