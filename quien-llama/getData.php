<?php
// Database connection info
$dbDetails = array(
    'host' => '127.0.0.1',
    'user' => 'spam_db',
    'pass' => 'SpamDB.0192',
    'db'   => 'spam_db'
);

// DB table to use
$table = 'tbl_directorio';

// Table's primary key
$primaryKey = 'pk';

// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database.
// The `dt` parameter represents the DataTables column identifier.
$columns = array(
    array( 'db' => 'telefono', 'dt' => 0 ),
    array( 'db' => 'entidad',  'dt' => 1 ),
    array(
        'db'        => 'telefono',
        'dt'        => 2,
    )
);

// Include SQL query processing class
require( 'clases/ssp.class.php' );

// Output data as json format
echo json_encode(
    SSP::simple( $_GET, $dbDetails, $table, $primaryKey, $columns )
);