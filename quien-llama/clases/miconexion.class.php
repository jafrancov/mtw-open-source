<?php

class miconexion extends mysqli {
    var $host = '127.0.0.1';
    var $user = 'spam_db';
    var $pass = 'SpamDB.0192';
    var $db = 'spam_db';

    public function __construct() {
        parent::__construct($this->host, $this->user, $this->pass, $this->db);

        if (mysqli_connect_error()) {
            die('Error de conexión (' . mysqli_connect_errno() . ') '
                . mysqli_connect_error());
        }
    }

    private function limpiaDatosTelefono($telefono, $entidad=null) {
        $tel = mb_convert_encoding(trim($telefono), 'UTF-8', 'UTF-8');
        $tel = (filter_var($tel, FILTER_SANITIZE_NUMBER_INT));
        $ent = mb_convert_encoding(trim($entidad), 'UTF-8', 'UTF-8');
        $ent = (filter_var($ent, FILTER_SANITIZE_STRING));
        $regexTelefono = '/^\d{10}$/';
        $response = array();

        if (!preg_match($regexTelefono, $tel)) {
            $response['error'] = True;
            $response['mensaje'] = 'Número inválido, por favor ingresa un número a 10 dígitos';
            echo json_encode($response, JSON_UNESCAPED_UNICODE);
            die();
        } else {
            return array('telefono' => $tel, 'entidad' => $ent);
        }
    }

    public function consulta($telefono) {
        $data = $this->limpiaDatosTelefono($telefono);
        $response['error'] = False;
//        $encontrado = mysqli::query('SELECT * FROM tbl_directorio WHERE telefono="' . $data['telefono'] . '"');
        $statement = $this->prepare("SELECT * FROM tbl_directorio WHERE telefono=?");
        $statement->bind_param("i", $data['telefono']);
        $statement->execute();
        $encontrado = $statement->get_result();

        if ($encontrado->num_rows == 0) {
            $response['encontrado'] = False;
        } else {
            $response['encontrado'] = True;
            $row = mysqli_fetch_assoc($encontrado);
            $response['quien_llama'] = $encontrado ? $row['entidad'] : False;
        }
        return $response;
    }

    public function insertar($telefono, $entidad) {
        $data = $this->limpiaDatosTelefono($telefono, $entidad);
//        mysqli::query('INSERT INTO tbl_directorio (telefono, entidad) VALUES ("' . $data['telefono'] . '", "' . $data['entidad'] . '")');
        $statement = $this->prepare('INSERT INTO tbl_directorio (telefono, entidad) VALUES (?, ?)');
        $statement->bind_param("is", $data['telefono'], $data['entidad']);
        $statement->execute();

        $response['error'] = False;
        $response['insertado'] = $statement->affected_rows > 0 ? True : False;
        return $response;
    }

    public function editar($telefono, $nueva_entidad) {
        $data = $this->limpiaDatosTelefono($telefono, $nueva_entidad);
//        mysqli::query('UPDATE tbl_directorio SET entidad="' . $data['entidad'] . '" WHERE telefono="' . $data['telefono'] . '"');
        $statement = $this->prepare('UPDATE tbl_directorio SET entidad=? WHERE telefono=?');
        $statement->bind_param("si", $data['entidad'], $data['telefono']);
        $statement->execute();

        $response['error'] = False;
        $response['actualizado'] = $statement->affected_rows > 0 ? True : False;
        return $response;
    }

    public function eliminar($telefono) {
        $data = $this->limpiaDatosTelefono($telefono);
//        mysqli::query('DELETE FROM tbl_directorio WHERE telefono="' . $data['telefono'] . '"');
        $statement = $this->prepare('DELETE FROM tbl_directorio WHERE telefono=?');
        $statement->bind_param("i", $data['telefono']);
        $statement->execute();

        $response['error'] = False;
        $response['telefono'] = $telefono;
        $response['eliminado'] = $statement->affected_rows > 0 ? True : False;
        return $response;
    }

    public function acceso($usuario, $password) {
        $user = mb_convert_encoding(trim($usuario), 'UTF-8', 'UTF-8');
        $user = (filter_var($user,  FILTER_SANITIZE_EMAIL));
        $pass = mb_convert_encoding(trim($password), 'UTF-8', 'UTF-8');
        $pass = htmlentities($pass, ENT_QUOTES, 'UTF-8');
        $result = mysqli::query('SELECT nombre, email FROM tbl_empleados WHERE email="' . $user . '" AND password="' . $pass . '"');

        if ($result->num_rows) {
            $registro = $result->fetch_array(MYSQLI_ASSOC);
            session_start();
            $_SESSION['email'] = $registro['email'];
            $_SESSION['nombre'] = $registro['nombre'];
            $_SESSION['auth'] = true;
        }
        return $result;
    }
}
?>
