<?php
if (isset($_GET['telefono'])) {
    $telefono = mb_convert_encoding(trim($_GET['telefono']), 'UTF-8', 'UTF-8');
    $telefono = htmlentities($telefono, ENT_QUOTES, 'UTF-8');

    require( 'clases/miconexion.class.php' );
    $conn = new miconexion();
    $response = $conn->consulta($telefono);
    $conn->close();
} else {
    header("Location: /");
}
?>
<style>
    input[type="text"] {
        text-align: center;
        font-size:26px;
    }
</style>
<form id="id-editar-telefono">
    <div class="form-group">
        <label for="id-telefono" class="col-form-label">Teléfono:</label>
        <input type="text" class="form-control" required readonly name="telefono" id="id-telefono" value="<?php echo $telefono ?>" />
    </div>
    <div class="form-group">
        <label for="id-quien-llama" class="col-form-label">¿Quién llama?</label>
        <input type="text" class="form-control" required name="quien-llama" id="id-quien-llama" value="<?php echo $response['quien_llama'] ?>" />
    </div>
    <div class="form-group text-right">
        <button type="button" id="btn_actualizar" class="btn btn-primary">Actualizar</button>
    </div>
</form>
<script type="text/javascript">
    $('#btn_actualizar').click(function () {
        var telefono = $("#id-editar-telefono input#id-telefono").val();
        var nuevoQuienLlama = $("#id-editar-telefono input#id-quien-llama").val();
        $.post("actualizar.php", $("#id-editar-telefono").serialize(), function( results ) {
            $('#consultaModal').modal('hide');
            if(results.error) {
                swal("¡ERROR!", results.mensaje, "error");
            } else if (!results.actualizado) {
                swal("¡OOPS!", "Tuvimos un problema para actualizar este registro, por favor intenta nuevamente", "error");
            } else {
                swal("¡Registro actualizado!", 'Actualizamos el número ' + telefono + ' como "' + nuevoQuienLlama + '"', "success");
            }
        }, "json");
        $('#example').DataTable().ajax.reload();
    })
</script>