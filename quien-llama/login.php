<?php
session_start();
if (!isset($_SESSION["email"])) {

    if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['recaptcha_response'])) {

        // Build POST request:
        $recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
        $recaptcha_secret = 'TU_LLAVE_PRIVADA';
        $recaptcha_response = $_POST['recaptcha_response'];

        // Make and decode POST request:
        $recaptcha = file_get_contents($recaptcha_url . '?secret=' . $recaptcha_secret . '&response=' . $recaptcha_response);
        $recaptcha = json_decode($recaptcha);

        // Take action based on the score returned:
        if ($recaptcha->score >= 0.5) {
            $usuario = $_POST['email'];
            $password = $_POST['password'];
            require('clases/miconexion.class.php');
            $conn = new miconexion();
            $existe = $conn->acceso($usuario, $password);
            $conn->close();
            if ($existe->num_rows) {
                header('Location: admin.php');
                exit;
            } else {
                $error = 'Nombre de usuario o contraseña incorrectos.';
            }
        } else {
            $error = 'Buen intento robot, pero no lo suficiente :)';
        }

    }
} else {
    header('Location: admin.php');
    exit;
}
?>
<!doctype html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Signin Template · Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="https://getbootstrap.com/docs/4.2/dist/css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">


    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">
</head>
<body class="text-center">
<form class="form-signin" method="post">
    <p style="color: #563d7d;"><i class="fas fa-blender-phone fa-5x"></i></p>
    <h1 class="h3 mb-3 font-weight-normal">Identifícate</h1>
    <label for="inputEmail" class="sr-only">Usuario</label>
    <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Usuario" required autofocus>
    <label for="inputPassword" class="sr-only">Contraseña</label>
    <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Contraseña" required>
    <input type="hidden" name="recaptcha_response" id="recaptchaResponse">
    <div class="checkbox mb-3 text-danger">
        <?php echo $error; ?>
    </div>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Ingresar</button>
    <p class="mt-5 mb-3 text-muted">&copy; 2019</p>
</form>
<script src="https://www.google.com/recaptcha/api.js?render=TU_LLAVE_PUBLICA"></script>
<script>
    grecaptcha.ready(function() {
        grecaptcha.execute('TU_LLAVE_PUBLICA', { action: '' }).then(function (token) {
            var recaptchaResponse = document.getElementById('recaptchaResponse');
            recaptchaResponse.value = token;
        });
    });
</script>
</body>
</html>
