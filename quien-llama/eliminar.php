<?php
if (isset($_GET['telefono'])) {
    $telefono = $_GET['telefono'];

    require( 'clases/miconexion.class.php' );
    $conn = new miconexion();
    echo json_encode($conn->eliminar($telefono), JSON_UNESCAPED_UNICODE);
    $conn->close();
} else {
    header("Location: /");
}
?>
