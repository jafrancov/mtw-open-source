<?php
if (isset($_POST['telefono']) && isset($_POST['quien-llama'])) {
    $telefono = $_POST['telefono'];
    $quienLlama = $_POST['quien-llama'];

    require( 'clases/miconexion.class.php' );
    $conn = new miconexion();
    echo json_encode($conn->editar($telefono, $quienLlama), JSON_UNESCAPED_UNICODE);
    $conn->close();
} else {
    header("Location: /");
}
?>