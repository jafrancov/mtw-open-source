<?php
    $numeroTelefono = "";
    $numeroTelefono = readline("Por favor escribe el número de teléfono ;-) : ");


     //Método if-else
//        if ($numeroTelefono){
//            if (strlen($numeroTelefono) == 10) {
//                if (is_numeric($numeroTelefono)) {
//                    echo $numeroTelefono;
//                } else {
//                    echo "Por favor ingresa valores numéricos\n";
//                }
//            } else {
//                echo "El número de teléfono debe ser a 10 dígitos\n";
//            }
//        } else {
//            echo "Por favor introduce un número de teléfono\n";
//        }

     // Método negativo
//        if (strlen($numeroTelefono) != 10) {
//            echo "Número inválido \n";
//            exit;
//        }
//
//        if (!is_numeric($numeroTelefono)) {
//            echo "Número no es numérico\n";
//            exit;
//        }

    // Método simplificado
//        if ((!is_numeric($numeroTelefono)) or (strlen($numeroTelefono) != 10)) {
//            echo "Número inválido\n";
//            exit;
//        }


    // Método con expresión regular https://www.phpliveregex.com/
    $regexTelefono = '/^\d{10}$/';
    if (!preg_match($regexTelefono, $numeroTelefono)) {
        echo "Número inválido\n";
        exit;
    }

    // http://php.net/manual/en/function.file-put-contents.php
    $numerosRegistrados = file_put_contents('numeros_registrados.txt', $numeroTelefono . PHP_EOL, FILE_APPEND);
    echo "Nuevo número registrado " . $numeroTelefono . PHP_EOL . PHP_EOL;

    // http://php.net/manual/en/function.file-get-contents.php
    $numerosRegistrados =  file_get_contents('numeros_registrados.txt');
    echo "Números registrados: " . PHP_EOL . $numerosRegistrados;
?>
